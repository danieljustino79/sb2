package com.sb2.endpoint.v1;

import com.sb2.persistence.model.Produto;

import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

@RestController
@RequestMapping("v1/produto")
public class ProdutoEndpoint {

    public List<Produto> lista = new ArrayList<Produto>();
    public ProdutoEndpoint() {
        //seed
        Produto produto = new Produto();
        produto.setId(1L);
        produto.setNome("Teclado");
        produto.setDescricao("ms");
        produto.setValor(99);
        produto.setValidade(Calendar.getInstance());

        lista.add(produto);

        produto = new Produto();
        produto.setId(2L);
        produto.setNome("Monitor");
        produto.setDescricao("AOC");
        produto.setValor(350.99);
        produto.setValidade(Calendar.getInstance());

        lista.add(produto);
    }
    private Produto obterPorIdService(long id) {
        Produto produtoAux = lista.stream().filter(x -> x.getId() == id).findFirst().orElse(null);
        return produtoAux;
    }
    private void adicionarService(Produto produto) {
        //produto.setId((long)lista.size() + 1);
        lista.add(produto);
    }
    private void editarService(Produto produto, Produto produtoOriginal) {
        int posicao = lista.indexOf(produtoOriginal);
        lista.remove(posicao);
        lista.add(produto);
    }
    private boolean removerService(long id) {
        Produto obj = obterPorIdService(id);
        if(obj == null) {
            return false;
        }
        else {
            int posicao = lista.indexOf(obj);
            lista.remove(posicao);
            return true;
        }
    }


    @GetMapping
    @ApiOperation(value="teste de string", notes="obs", response = String.class)
    public String index(){
        return "ok google Endpoint";
    }

    @GetMapping(path="listar")
    @ApiOperation(value="Obter listagem", notes="obs", response = Produto[].class)
    public List<Produto> listar(){
        return lista;
    }

    @GetMapping(path="{id}")
    @ApiOperation(value="Obter um produto", notes="passe o id na final da url", response = Produto.class)
    public Produto obterPorId(@PathVariable long id){
        Produto produto = obterPorIdService(id);
        return produto;
    }

    @ApiOperation(value = "Adicionar novo produto", response = Produto.class)
    @PostMapping
    public Produto adicionar(@RequestBody Produto produto) {
        adicionarService(produto);
        return produto;
    }

    @ApiOperation(value = "Editar um produto")
    @PutMapping
    public Produto editar(@RequestBody Produto produto) {
        Produto produtoOriginal = obterPorIdService(produto.getId());

        if(produtoOriginal == null) {
            produtoOriginal = new Produto();
            produtoOriginal.setNome("erro");
            return produtoOriginal;
        }

        editarService(produto, produtoOriginal);

        return produto;
    }

    @ApiOperation(value = "Remover um produto")
    @DeleteMapping(path = "{id}")
    public boolean remover(@PathVariable long id) {
        return removerService(id);
    }
}
