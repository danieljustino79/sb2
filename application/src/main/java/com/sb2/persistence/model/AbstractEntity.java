package com.sb2.persistence.model;

import com.sun.javafx.beans.IDProperty;

public class AbstractEntity {
    protected Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


}
