package com.sb2.persistence.model;

import java.util.Calendar;

public class Produto extends AbstractEntity {
    private String nome;
    private String descricao;

    private double valor;

    //@DateTimeFormat(pattern="dd/MM/yyyy")
    private Calendar Validade;

    public String getNome() {
        return nome;
    }
    public void setNome(String nome) {
        this.nome = nome;
    }
    public String getDescricao() {
        return descricao;
    }
    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }
    public double getValor() {
        return valor;
    }
    public void setValor(double valor) {
        this.valor = valor;
    }
    public Calendar getValidade() {
        return Validade;
    }
    public void setValidade(Calendar validade) {
        Validade = validade;
    }
}
